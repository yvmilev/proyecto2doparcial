﻿
namespace proyecto_ps
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textCasa = new System.Windows.Forms.TextBox();
            this.textRepre = new System.Windows.Forms.TextBox();
            this.textVisita = new System.Windows.Forms.TextBox();
            this.textVehiculo = new System.Windows.Forms.TextBox();
            this.textPlacas = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.numcasaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombrerepDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.motivovisitaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehiculoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.placasDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ingresosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.puerta_serenaDataSet = new proyecto_ps.puerta_serenaDataSet();
            this.ingresosTableAdapter = new proyecto_ps.puerta_serenaDataSetTableAdapters.ingresosTableAdapter();
            this.textId = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ingresosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.puerta_serenaDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(203, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Número de casa que visita: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(206, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre de Representante: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Motivo de Visita:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 20);
            this.label4.TabIndex = 3;
            this.label4.Text = "ID:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 157);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 20);
            this.label5.TabIndex = 4;
            this.label5.Text = "Vehículo:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(205, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(275, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Visitante | Puerta Serena";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 20);
            this.label7.TabIndex = 6;
            this.label7.Text = "Placas:";
            // 
            // textCasa
            // 
            this.textCasa.Location = new System.Drawing.Point(221, 55);
            this.textCasa.Name = "textCasa";
            this.textCasa.Size = new System.Drawing.Size(281, 20);
            this.textCasa.TabIndex = 7;
            // 
            // textRepre
            // 
            this.textRepre.Location = new System.Drawing.Point(221, 81);
            this.textRepre.Name = "textRepre";
            this.textRepre.Size = new System.Drawing.Size(281, 20);
            this.textRepre.TabIndex = 8;
            // 
            // textVisita
            // 
            this.textVisita.Location = new System.Drawing.Point(221, 107);
            this.textVisita.Name = "textVisita";
            this.textVisita.Size = new System.Drawing.Size(281, 20);
            this.textVisita.TabIndex = 9;
            // 
            // textVehiculo
            // 
            this.textVehiculo.Location = new System.Drawing.Point(221, 159);
            this.textVehiculo.Name = "textVehiculo";
            this.textVehiculo.Size = new System.Drawing.Size(281, 20);
            this.textVehiculo.TabIndex = 10;
            // 
            // textPlacas
            // 
            this.textPlacas.Location = new System.Drawing.Point(221, 183);
            this.textPlacas.Name = "textPlacas";
            this.textPlacas.Size = new System.Drawing.Size(281, 20);
            this.textPlacas.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(558, 120);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Guardar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numcasaDataGridViewTextBoxColumn,
            this.nombrerepDataGridViewTextBoxColumn,
            this.motivovisitaDataGridViewTextBoxColumn,
            this.idDataGridViewTextBoxColumn,
            this.vehiculoDataGridViewTextBoxColumn,
            this.placasDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.ingresosBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 264);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(644, 150);
            this.dataGridView1.TabIndex = 13;
            // 
            // numcasaDataGridViewTextBoxColumn
            // 
            this.numcasaDataGridViewTextBoxColumn.DataPropertyName = "numcasa";
            this.numcasaDataGridViewTextBoxColumn.HeaderText = "numcasa";
            this.numcasaDataGridViewTextBoxColumn.Name = "numcasaDataGridViewTextBoxColumn";
            // 
            // nombrerepDataGridViewTextBoxColumn
            // 
            this.nombrerepDataGridViewTextBoxColumn.DataPropertyName = "nombrerep";
            this.nombrerepDataGridViewTextBoxColumn.HeaderText = "nombrerep";
            this.nombrerepDataGridViewTextBoxColumn.Name = "nombrerepDataGridViewTextBoxColumn";
            // 
            // motivovisitaDataGridViewTextBoxColumn
            // 
            this.motivovisitaDataGridViewTextBoxColumn.DataPropertyName = "motivovisita";
            this.motivovisitaDataGridViewTextBoxColumn.HeaderText = "motivovisita";
            this.motivovisitaDataGridViewTextBoxColumn.Name = "motivovisitaDataGridViewTextBoxColumn";
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            // 
            // vehiculoDataGridViewTextBoxColumn
            // 
            this.vehiculoDataGridViewTextBoxColumn.DataPropertyName = "vehiculo";
            this.vehiculoDataGridViewTextBoxColumn.HeaderText = "vehiculo";
            this.vehiculoDataGridViewTextBoxColumn.Name = "vehiculoDataGridViewTextBoxColumn";
            // 
            // placasDataGridViewTextBoxColumn
            // 
            this.placasDataGridViewTextBoxColumn.DataPropertyName = "placas";
            this.placasDataGridViewTextBoxColumn.HeaderText = "placas";
            this.placasDataGridViewTextBoxColumn.Name = "placasDataGridViewTextBoxColumn";
            // 
            // ingresosBindingSource
            // 
            this.ingresosBindingSource.DataMember = "ingresos";
            this.ingresosBindingSource.DataSource = this.puerta_serenaDataSet;
            // 
            // puerta_serenaDataSet
            // 
            this.puerta_serenaDataSet.DataSetName = "puerta_serenaDataSet";
            this.puerta_serenaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ingresosTableAdapter
            // 
            this.ingresosTableAdapter.ClearBeforeFill = true;
            // 
            // textId
            // 
            this.textId.Location = new System.Drawing.Point(221, 133);
            this.textId.Name = "textId";
            this.textId.Size = new System.Drawing.Size(281, 20);
            this.textId.TabIndex = 14;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 424);
            this.Controls.Add(this.textId);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textPlacas);
            this.Controls.Add(this.textVehiculo);
            this.Controls.Add(this.textVisita);
            this.Controls.Add(this.textRepre);
            this.Controls.Add(this.textCasa);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ingresosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.puerta_serenaDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textCasa;
        private System.Windows.Forms.TextBox textRepre;
        private System.Windows.Forms.TextBox textVisita;
        private System.Windows.Forms.TextBox textVehiculo;
        private System.Windows.Forms.TextBox textPlacas;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private puerta_serenaDataSet puerta_serenaDataSet;
        private System.Windows.Forms.BindingSource ingresosBindingSource;
        private puerta_serenaDataSetTableAdapters.ingresosTableAdapter ingresosTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn numcasaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombrerepDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn motivovisitaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vehiculoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn placasDataGridViewTextBoxColumn;
        private System.Windows.Forms.TextBox textId;
    }
}