﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace proyecto_ps
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void ingresosBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.ingresosBindingSource.EndEdit();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'puerta_serenaDataSet.ingresos' Puede moverla o quitarla según sea necesario.
            this.ingresosTableAdapter.Fill(this.puerta_serenaDataSet.ingresos);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.ingresosTableAdapter.Insertar(textCasa.Text, textRepre.Text, textVisita.Text, textId.Text, textVehiculo.Text, textPlacas.Text);
            this.ingresosTableAdapter.Fill(this.puerta_serenaDataSet.ingresos);
        }
    }
}
