﻿
namespace proyecto_ps
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label numcasaLabel;
            System.Windows.Forms.Label nombrerepLabel;
            System.Windows.Forms.Label motivovisitaLabel;
            System.Windows.Forms.Label idLabel;
            System.Windows.Forms.Label vehiculoLabel;
            System.Windows.Forms.Label placasLabel;
            this.label3 = new System.Windows.Forms.Label();
            this.puerta_serenaDataSet = new proyecto_ps.puerta_serenaDataSet();
            this.ingresosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ingresosTableAdapter = new proyecto_ps.puerta_serenaDataSetTableAdapters.ingresosTableAdapter();
            this.tableAdapterManager = new proyecto_ps.puerta_serenaDataSetTableAdapters.TableAdapterManager();
            this.numcasaTextBox = new System.Windows.Forms.TextBox();
            this.nombrerepTextBox = new System.Windows.Forms.TextBox();
            this.motivovisitaTextBox = new System.Windows.Forms.TextBox();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.vehiculoTextBox = new System.Windows.Forms.TextBox();
            this.placasTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            numcasaLabel = new System.Windows.Forms.Label();
            nombrerepLabel = new System.Windows.Forms.Label();
            motivovisitaLabel = new System.Windows.Forms.Label();
            idLabel = new System.Windows.Forms.Label();
            vehiculoLabel = new System.Windows.Forms.Label();
            placasLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.puerta_serenaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ingresosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Script", 24.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(66, 9);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(342, 52);
            this.label3.TabIndex = 6;
            this.label3.Text = "Registro de visitas";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // puerta_serenaDataSet
            // 
            this.puerta_serenaDataSet.DataSetName = "puerta_serenaDataSet";
            this.puerta_serenaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ingresosBindingSource
            // 
            this.ingresosBindingSource.DataMember = "ingresos";
            this.ingresosBindingSource.DataSource = this.puerta_serenaDataSet;
            // 
            // ingresosTableAdapter
            // 
            this.ingresosTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ingresosTableAdapter = this.ingresosTableAdapter;
            this.tableAdapterManager.UpdateOrder = proyecto_ps.puerta_serenaDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.usuariosTableAdapter = null;
            // 
            // numcasaLabel
            // 
            numcasaLabel.AutoSize = true;
            numcasaLabel.Location = new System.Drawing.Point(12, 69);
            numcasaLabel.Name = "numcasaLabel";
            numcasaLabel.Size = new System.Drawing.Size(238, 24);
            numcasaLabel.TabIndex = 7;
            numcasaLabel.Text = "Número de casa que visita:";
            // 
            // numcasaTextBox
            // 
            this.numcasaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ingresosBindingSource, "numcasa", true));
            this.numcasaTextBox.Location = new System.Drawing.Point(256, 69);
            this.numcasaTextBox.Name = "numcasaTextBox";
            this.numcasaTextBox.Size = new System.Drawing.Size(100, 29);
            this.numcasaTextBox.TabIndex = 8;
            // 
            // nombrerepLabel
            // 
            nombrerepLabel.AutoSize = true;
            nombrerepLabel.Location = new System.Drawing.Point(12, 104);
            nombrerepLabel.Name = "nombrerepLabel";
            nombrerepLabel.Size = new System.Drawing.Size(210, 24);
            nombrerepLabel.TabIndex = 9;
            nombrerepLabel.Text = "Representante de visita:";
            // 
            // nombrerepTextBox
            // 
            this.nombrerepTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ingresosBindingSource, "nombrerep", true));
            this.nombrerepTextBox.Location = new System.Drawing.Point(256, 101);
            this.nombrerepTextBox.Name = "nombrerepTextBox";
            this.nombrerepTextBox.Size = new System.Drawing.Size(100, 29);
            this.nombrerepTextBox.TabIndex = 10;
            // 
            // motivovisitaLabel
            // 
            motivovisitaLabel.AutoSize = true;
            motivovisitaLabel.Location = new System.Drawing.Point(12, 139);
            motivovisitaLabel.Name = "motivovisitaLabel";
            motivovisitaLabel.Size = new System.Drawing.Size(142, 24);
            motivovisitaLabel.TabIndex = 11;
            motivovisitaLabel.Text = "Motivo de visita:";
            // 
            // motivovisitaTextBox
            // 
            this.motivovisitaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ingresosBindingSource, "motivovisita", true));
            this.motivovisitaTextBox.Location = new System.Drawing.Point(160, 139);
            this.motivovisitaTextBox.Name = "motivovisitaTextBox";
            this.motivovisitaTextBox.Size = new System.Drawing.Size(196, 29);
            this.motivovisitaTextBox.TabIndex = 12;
            // 
            // idLabel
            // 
            idLabel.AutoSize = true;
            idLabel.Location = new System.Drawing.Point(12, 174);
            idLabel.Name = "idLabel";
            idLabel.Size = new System.Drawing.Size(153, 24);
            idLabel.TabIndex = 13;
            idLabel.Text = "Se identifica con:";
            // 
            // idTextBox
            // 
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ingresosBindingSource, "id", true));
            this.idTextBox.Location = new System.Drawing.Point(160, 171);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.Size = new System.Drawing.Size(196, 29);
            this.idTextBox.TabIndex = 14;
            // 
            // vehiculoLabel
            // 
            vehiculoLabel.AutoSize = true;
            vehiculoLabel.Location = new System.Drawing.Point(12, 209);
            vehiculoLabel.Name = "vehiculoLabel";
            vehiculoLabel.Size = new System.Drawing.Size(90, 24);
            vehiculoLabel.TabIndex = 15;
            vehiculoLabel.Text = "Vehiculo:";
            // 
            // vehiculoTextBox
            // 
            this.vehiculoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ingresosBindingSource, "vehiculo", true));
            this.vehiculoTextBox.Location = new System.Drawing.Point(128, 206);
            this.vehiculoTextBox.Name = "vehiculoTextBox";
            this.vehiculoTextBox.Size = new System.Drawing.Size(228, 29);
            this.vehiculoTextBox.TabIndex = 16;
            // 
            // placasLabel
            // 
            placasLabel.AutoSize = true;
            placasLabel.Location = new System.Drawing.Point(12, 244);
            placasLabel.Name = "placasLabel";
            placasLabel.Size = new System.Drawing.Size(69, 24);
            placasLabel.TabIndex = 17;
            placasLabel.Text = "placas:";
            // 
            // placasTextBox
            // 
            this.placasTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ingresosBindingSource, "placas", true));
            this.placasTextBox.Location = new System.Drawing.Point(128, 241);
            this.placasTextBox.Name = "placasTextBox";
            this.placasTextBox.Size = new System.Drawing.Size(228, 29);
            this.placasTextBox.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(22, 279);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 39);
            this.button1.TabIndex = 19;
            this.button1.Text = "Gurdar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 330);
            this.Controls.Add(this.button1);
            this.Controls.Add(numcasaLabel);
            this.Controls.Add(this.numcasaTextBox);
            this.Controls.Add(nombrerepLabel);
            this.Controls.Add(this.nombrerepTextBox);
            this.Controls.Add(motivovisitaLabel);
            this.Controls.Add(this.motivovisitaTextBox);
            this.Controls.Add(idLabel);
            this.Controls.Add(this.idTextBox);
            this.Controls.Add(vehiculoLabel);
            this.Controls.Add(this.vehiculoTextBox);
            this.Controls.Add(placasLabel);
            this.Controls.Add(this.placasTextBox);
            this.Controls.Add(this.label3);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Form2";
            this.Text = "Puerta Serena-Registro de visita";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.puerta_serenaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ingresosBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private puerta_serenaDataSet puerta_serenaDataSet;
        private System.Windows.Forms.BindingSource ingresosBindingSource;
        private puerta_serenaDataSetTableAdapters.ingresosTableAdapter ingresosTableAdapter;
        private puerta_serenaDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox numcasaTextBox;
        private System.Windows.Forms.TextBox nombrerepTextBox;
        private System.Windows.Forms.TextBox motivovisitaTextBox;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox vehiculoTextBox;
        private System.Windows.Forms.TextBox placasTextBox;
        private System.Windows.Forms.Button button1;
    }
}