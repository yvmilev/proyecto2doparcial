﻿
namespace ps8
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label visitanteLabel;
            System.Windows.Forms.Label nombreLabel;
            System.Windows.Forms.Label horaLabel;
            System.Windows.Forms.Label motivoLabel;
            System.Windows.Forms.Label identLabel;
            System.Windows.Forms.Label fechaLabel;
            System.Windows.Forms.Label lugarLabel;
            System.Windows.Forms.Label autoLabel;
            System.Windows.Forms.Label placasLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.ps5BindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.ps5BindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.ps5DataGridView = new System.Windows.Forms.DataGridView();
            this.visitanteTextBox = new System.Windows.Forms.TextBox();
            this.nombreTextBox = new System.Windows.Forms.TextBox();
            this.horaTextBox = new System.Windows.Forms.TextBox();
            this.motivoTextBox = new System.Windows.Forms.TextBox();
            this.identTextBox = new System.Windows.Forms.TextBox();
            this.fechaTextBox = new System.Windows.Forms.TextBox();
            this.lugarTextBox = new System.Windows.Forms.TextBox();
            this.autoTextBox = new System.Windows.Forms.TextBox();
            this.placasTextBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.ps5BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this._ps5_0DataSet = new ps8._ps5_0DataSet();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ps5TableAdapter = new ps8._ps5_0DataSetTableAdapters.ps5TableAdapter();
            this.tableAdapterManager = new ps8._ps5_0DataSetTableAdapters.TableAdapterManager();
            visitanteLabel = new System.Windows.Forms.Label();
            nombreLabel = new System.Windows.Forms.Label();
            horaLabel = new System.Windows.Forms.Label();
            motivoLabel = new System.Windows.Forms.Label();
            identLabel = new System.Windows.Forms.Label();
            fechaLabel = new System.Windows.Forms.Label();
            lugarLabel = new System.Windows.Forms.Label();
            autoLabel = new System.Windows.Forms.Label();
            placasLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ps5BindingNavigator)).BeginInit();
            this.ps5BindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ps5DataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ps5BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._ps5_0DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // visitanteLabel
            // 
            visitanteLabel.AutoSize = true;
            visitanteLabel.Location = new System.Drawing.Point(5, 44);
            visitanteLabel.Name = "visitanteLabel";
            visitanteLabel.Size = new System.Drawing.Size(50, 13);
            visitanteLabel.TabIndex = 2;
            visitanteLabel.Text = "Visitante:";
            // 
            // nombreLabel
            // 
            nombreLabel.AutoSize = true;
            nombreLabel.Location = new System.Drawing.Point(8, 70);
            nombreLabel.Name = "nombreLabel";
            nombreLabel.Size = new System.Drawing.Size(47, 13);
            nombreLabel.TabIndex = 4;
            nombreLabel.Text = "Nombre:";
            // 
            // horaLabel
            // 
            horaLabel.AutoSize = true;
            horaLabel.Location = new System.Drawing.Point(22, 96);
            horaLabel.Name = "horaLabel";
            horaLabel.Size = new System.Drawing.Size(33, 13);
            horaLabel.TabIndex = 6;
            horaLabel.Text = "Hora:";
            // 
            // motivoLabel
            // 
            motivoLabel.AutoSize = true;
            motivoLabel.Location = new System.Drawing.Point(13, 122);
            motivoLabel.Name = "motivoLabel";
            motivoLabel.Size = new System.Drawing.Size(42, 13);
            motivoLabel.TabIndex = 8;
            motivoLabel.Text = "Motivo:";
            // 
            // identLabel
            // 
            identLabel.AutoSize = true;
            identLabel.Location = new System.Drawing.Point(21, 148);
            identLabel.Name = "identLabel";
            identLabel.Size = new System.Drawing.Size(34, 13);
            identLabel.TabIndex = 10;
            identLabel.Text = "Ident:";
            // 
            // fechaLabel
            // 
            fechaLabel.AutoSize = true;
            fechaLabel.Location = new System.Drawing.Point(15, 174);
            fechaLabel.Name = "fechaLabel";
            fechaLabel.Size = new System.Drawing.Size(40, 13);
            fechaLabel.TabIndex = 12;
            fechaLabel.Text = "Fecha:";
            // 
            // lugarLabel
            // 
            lugarLabel.AutoSize = true;
            lugarLabel.Location = new System.Drawing.Point(18, 200);
            lugarLabel.Name = "lugarLabel";
            lugarLabel.Size = new System.Drawing.Size(37, 13);
            lugarLabel.TabIndex = 14;
            lugarLabel.Text = "Lugar:";
            // 
            // autoLabel
            // 
            autoLabel.AutoSize = true;
            autoLabel.Location = new System.Drawing.Point(171, 44);
            autoLabel.Name = "autoLabel";
            autoLabel.Size = new System.Drawing.Size(32, 13);
            autoLabel.TabIndex = 16;
            autoLabel.Text = "Auto:";
            // 
            // placasLabel
            // 
            placasLabel.AutoSize = true;
            placasLabel.Location = new System.Drawing.Point(161, 70);
            placasLabel.Name = "placasLabel";
            placasLabel.Size = new System.Drawing.Size(42, 13);
            placasLabel.TabIndex = 18;
            placasLabel.Text = "Placas:";
            // 
            // ps5BindingNavigator
            // 
            this.ps5BindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.ps5BindingNavigator.BindingSource = this.ps5BindingSource;
            this.ps5BindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.ps5BindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.ps5BindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.ps5BindingNavigatorSaveItem});
            this.ps5BindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.ps5BindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.ps5BindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.ps5BindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.ps5BindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.ps5BindingNavigator.Name = "ps5BindingNavigator";
            this.ps5BindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.ps5BindingNavigator.Size = new System.Drawing.Size(471, 25);
            this.ps5BindingNavigator.TabIndex = 0;
            this.ps5BindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Agregar nuevo";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(37, 22);
            this.bindingNavigatorCountItem.Text = "de {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Número total de elementos";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Eliminar";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Mover primero";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Mover anterior";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Posición";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Posición actual";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Mover siguiente";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Mover último";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // ps5BindingNavigatorSaveItem
            // 
            this.ps5BindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ps5BindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("ps5BindingNavigatorSaveItem.Image")));
            this.ps5BindingNavigatorSaveItem.Name = "ps5BindingNavigatorSaveItem";
            this.ps5BindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
            this.ps5BindingNavigatorSaveItem.Text = "Guardar datos";
            this.ps5BindingNavigatorSaveItem.Click += new System.EventHandler(this.ps5BindingNavigatorSaveItem_Click);
            // 
            // ps5DataGridView
            // 
            this.ps5DataGridView.AutoGenerateColumns = false;
            this.ps5DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ps5DataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.ps5DataGridView.DataSource = this.ps5BindingSource;
            this.ps5DataGridView.Location = new System.Drawing.Point(12, 218);
            this.ps5DataGridView.Name = "ps5DataGridView";
            this.ps5DataGridView.Size = new System.Drawing.Size(439, 220);
            this.ps5DataGridView.TabIndex = 1;
            // 
            // visitanteTextBox
            // 
            this.visitanteTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Visitante", true));
            this.visitanteTextBox.Location = new System.Drawing.Point(61, 41);
            this.visitanteTextBox.Name = "visitanteTextBox";
            this.visitanteTextBox.Size = new System.Drawing.Size(100, 20);
            this.visitanteTextBox.TabIndex = 3;
            // 
            // nombreTextBox
            // 
            this.nombreTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Nombre", true));
            this.nombreTextBox.Location = new System.Drawing.Point(61, 67);
            this.nombreTextBox.Name = "nombreTextBox";
            this.nombreTextBox.Size = new System.Drawing.Size(100, 20);
            this.nombreTextBox.TabIndex = 5;
            // 
            // horaTextBox
            // 
            this.horaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Hora", true));
            this.horaTextBox.Location = new System.Drawing.Point(61, 93);
            this.horaTextBox.Name = "horaTextBox";
            this.horaTextBox.Size = new System.Drawing.Size(100, 20);
            this.horaTextBox.TabIndex = 7;
            // 
            // motivoTextBox
            // 
            this.motivoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Motivo", true));
            this.motivoTextBox.Location = new System.Drawing.Point(61, 119);
            this.motivoTextBox.Name = "motivoTextBox";
            this.motivoTextBox.Size = new System.Drawing.Size(100, 20);
            this.motivoTextBox.TabIndex = 9;
            // 
            // identTextBox
            // 
            this.identTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Ident", true));
            this.identTextBox.Location = new System.Drawing.Point(61, 145);
            this.identTextBox.Name = "identTextBox";
            this.identTextBox.Size = new System.Drawing.Size(100, 20);
            this.identTextBox.TabIndex = 11;
            // 
            // fechaTextBox
            // 
            this.fechaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Fecha", true));
            this.fechaTextBox.Location = new System.Drawing.Point(61, 171);
            this.fechaTextBox.Name = "fechaTextBox";
            this.fechaTextBox.Size = new System.Drawing.Size(100, 20);
            this.fechaTextBox.TabIndex = 13;
            // 
            // lugarTextBox
            // 
            this.lugarTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Lugar", true));
            this.lugarTextBox.Location = new System.Drawing.Point(61, 197);
            this.lugarTextBox.Name = "lugarTextBox";
            this.lugarTextBox.Size = new System.Drawing.Size(100, 20);
            this.lugarTextBox.TabIndex = 15;
            // 
            // autoTextBox
            // 
            this.autoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Auto", true));
            this.autoTextBox.Location = new System.Drawing.Point(209, 41);
            this.autoTextBox.Name = "autoTextBox";
            this.autoTextBox.Size = new System.Drawing.Size(100, 20);
            this.autoTextBox.TabIndex = 17;
            // 
            // placasTextBox
            // 
            this.placasTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.ps5BindingSource, "Placas", true));
            this.placasTextBox.Location = new System.Drawing.Point(209, 67);
            this.placasTextBox.Name = "placasTextBox";
            this.placasTextBox.Size = new System.Drawing.Size(100, 20);
            this.placasTextBox.TabIndex = 19;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(209, 96);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 39);
            this.button1.TabIndex = 20;
            this.button1.Text = "Ingresar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-17, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(488, 279);
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(209, 141);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 46);
            this.button2.TabIndex = 22;
            this.button2.Text = "Borrar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(315, 96);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(97, 39);
            this.button3.TabIndex = 23;
            this.button3.Text = "Buscar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(315, 138);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(97, 49);
            this.button4.TabIndex = 24;
            this.button4.Text = "Editar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // ps5BindingSource
            // 
            this.ps5BindingSource.DataMember = "ps5";
            this.ps5BindingSource.DataSource = this._ps5_0DataSet;
            // 
            // _ps5_0DataSet
            // 
            this._ps5_0DataSet.DataSetName = "_ps5_0DataSet";
            this._ps5_0DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "Visitante";
            this.dataGridViewTextBoxColumn1.HeaderText = "Visitante";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nombre";
            this.dataGridViewTextBoxColumn2.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Hora";
            this.dataGridViewTextBoxColumn3.HeaderText = "Hora";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Motivo";
            this.dataGridViewTextBoxColumn4.HeaderText = "Motivo";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "Ident";
            this.dataGridViewTextBoxColumn5.HeaderText = "Ident";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Fecha";
            this.dataGridViewTextBoxColumn6.HeaderText = "Fecha";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Lugar";
            this.dataGridViewTextBoxColumn7.HeaderText = "Lugar";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Auto";
            this.dataGridViewTextBoxColumn8.HeaderText = "Auto";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "Placas";
            this.dataGridViewTextBoxColumn9.HeaderText = "Placas";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            // 
            // ps5TableAdapter
            // 
            this.ps5TableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ps5TableAdapter = this.ps5TableAdapter;
            this.tableAdapterManager.UpdateOrder = ps8._ps5_0DataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 458);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(placasLabel);
            this.Controls.Add(this.placasTextBox);
            this.Controls.Add(autoLabel);
            this.Controls.Add(this.autoTextBox);
            this.Controls.Add(lugarLabel);
            this.Controls.Add(this.lugarTextBox);
            this.Controls.Add(fechaLabel);
            this.Controls.Add(this.fechaTextBox);
            this.Controls.Add(identLabel);
            this.Controls.Add(this.identTextBox);
            this.Controls.Add(motivoLabel);
            this.Controls.Add(this.motivoTextBox);
            this.Controls.Add(horaLabel);
            this.Controls.Add(this.horaTextBox);
            this.Controls.Add(nombreLabel);
            this.Controls.Add(this.nombreTextBox);
            this.Controls.Add(visitanteLabel);
            this.Controls.Add(this.visitanteTextBox);
            this.Controls.Add(this.ps5DataGridView);
            this.Controls.Add(this.ps5BindingNavigator);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form2";
            this.Text = "Registro";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ps5BindingNavigator)).EndInit();
            this.ps5BindingNavigator.ResumeLayout(false);
            this.ps5BindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ps5DataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ps5BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._ps5_0DataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private _ps5_0DataSet _ps5_0DataSet;
        private System.Windows.Forms.BindingSource ps5BindingSource;
        private _ps5_0DataSetTableAdapters.ps5TableAdapter ps5TableAdapter;
        private _ps5_0DataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator ps5BindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton ps5BindingNavigatorSaveItem;
        private System.Windows.Forms.DataGridView ps5DataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.TextBox visitanteTextBox;
        private System.Windows.Forms.TextBox nombreTextBox;
        private System.Windows.Forms.TextBox horaTextBox;
        private System.Windows.Forms.TextBox motivoTextBox;
        private System.Windows.Forms.TextBox identTextBox;
        private System.Windows.Forms.TextBox fechaTextBox;
        private System.Windows.Forms.TextBox lugarTextBox;
        private System.Windows.Forms.TextBox autoTextBox;
        private System.Windows.Forms.TextBox placasTextBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}