﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ps8
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void ps5BindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.ps5BindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this._ps5_0DataSet);

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla '_ps5_0DataSet.ps5' Puede moverla o quitarla según sea necesario.
            this.ps5TableAdapter.Fill(this._ps5_0DataSet.ps5);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.ps5TableAdapter.insertar(nombreTextBox.Text, horaTextBox.Text, motivoTextBox.Text, identTextBox.Text, fechaTextBox.Text, lugarTextBox.Text, autoTextBox.Text, placasTextBox.Text);
            this.ps5TableAdapter.Fill(this._ps5_0DataSet.ps5);
            nombreTextBox.Text = "";
            horaTextBox.Text = "";
            motivoTextBox.Text = "";
            identTextBox.Text = "";
            fechaTextBox.Text = "";
            lugarTextBox.Text = "";
            autoTextBox.Text = "";
            placasTextBox.Text = "";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.ps5TableAdapter.buscar(this._ps5_0DataSet.ps5, nombreTextBox.Text);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.ps5TableAdapter.eliminar(nombreTextBox.Text);
            this.ps5TableAdapter.Fill(this._ps5_0DataSet.ps5);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.ps5TableAdapter.editar(nombreTextBox.Text, horaTextBox.Text, motivoTextBox.Text, identTextBox.Text, fechaTextBox.Text, lugarTextBox.Text, autoTextBox.Text, placasTextBox.Text, nombreTextBox.Text);
            this.ps5TableAdapter.Fill(this._ps5_0DataSet.ps5);
        }
    }
}
