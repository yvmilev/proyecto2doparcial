﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ps8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Iniciar_Click(object sender, EventArgs e)
        {
            int intentos = 0;
            string[,] arreglo = { { "kevin", "yami" }, { "6553", "2016" } };
            if (usuario.Text == arreglo[0, 0] && contraseña.Text == arreglo[1, 0])
            {
                this.Hide();
                Form2 p = new Form2();
                p.Show();
            }
            else if (intentos == 3)
            {
                MessageBox.Show("Ha alcanzado el nivel maximo de intentos, Porfavor vuelva a intentarlo", "Mensaje del sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            else
            {

                intentos += 1;
                MessageBox.Show("Error, no se encuentra el usuario. Puede que no esté registrado o se haya deshabilitado", "Mensaje del sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                contraseña.Text = "";
                this.Hide();
                Form2 p = new Form2();
                p.Show();

            }


        }
    }
}
